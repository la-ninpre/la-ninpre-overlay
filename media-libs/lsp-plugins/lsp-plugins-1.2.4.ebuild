# Copyright 2019-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit xdg

DESCRIPTION="Linux Studio Plugins"
HOMEPAGE="https://lsp-plug.in"

SRC_URI="https://github.com/sadko4u/lsp-plugins/releases/download/${PV}/${PN}-src-${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="amd64 ~arm ~arm64 ~ppc ~ppc64 x86"
S=${WORKDIR}/${PN}

LICENSE="LGPL-3"
SLOT="0"
IUSE="doc jack ladspa +lv2 test"
REQUIRED_USE="|| ( jack ladspa lv2 )
	test? ( jack )"

RESTRICT="!test? ( test )"

DEPEND="
	dev-libs/expat
	media-libs/libsndfile
	media-libs/libglvnd[X]
	doc? ( dev-lang/php:* )
	jack? (
		virtual/jack
		x11-libs/cairo[X]
	)
	ladspa? ( media-libs/ladspa-sdk )
	lv2? (
		media-libs/lv2
		x11-libs/cairo[X]
	)
"
RDEPEND="${DEPEND}"
BDEPEND=""

PATCHES=(
	"${FILESDIR}/${P}-r3d-glx-libdir.patch"
)

src_configure() {
	use doc && MODULES+="doc"
	use jack && MODULES+=" jack"
	use ladspa && MODULES+=" ladspa"
	use lv2 && MODULES+=" lv2"
	emake FEATURES="${MODULES}" PREFIX="/usr" LIBDIR="/usr/$(get_libdir)" config
}

src_install() {
	emake DESTDIR="${ED}" install
}
