# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="DSP library for signal processing"

HOMEPAGE="https://github.com/lsp-plugins/lsp-dsp-lib"

LSP_COMMON_LIB_PV="1.0.21"
LSP_TEST_FW_PV="1.0.15"
SRC_URI="
	https://github.com/lsp-plugins/${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz
	https://github.com/lsp-plugins/lsp-common-lib/archive/refs/tags/${LSP_COMMON_LIB_PV}.tar.gz -> lsp-common-lib-${LSP_COMMON_LIB_PV}.tar.gz
	https://github.com/lsp-plugins/lsp-test-fw/archive/refs/tags/${LSP_TEST_FW_PV}.tar.gz -> lsp-test-fw-${LSP_TEST_FW_PV}.tar.gz
"

LICENSE="LGPL-3+"

SLOT="0"

KEYWORDS="~amd64"

IUSE=""

src_unpack() {
	default
	mkdir "${S}/modules"
	mv "${WORKDIR}/lsp-common-lib-${LSP_COMMON_LIB_PV}" "${S}/modules/lsp-common-lib"
	mv "${WORKDIR}/lsp-test-fw-${LSP_TEST_FW_PV}" "${S}/modules/lsp-test-fw"
}

src_configure() {
	emake PREFIX="/usr" LIBDIR="/usr/$(get_libdir)" config
}
