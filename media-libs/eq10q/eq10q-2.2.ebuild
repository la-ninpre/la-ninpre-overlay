# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="audio plugin bundle implementing a powerful and flexible parametric equalizer and more"

HOMEPAGE="http://eq10q.sourceforge.net/"

SRC_URI="https://sourceforge.net/projects/${PN}/files/${P}.tar.gz"

LICENSE="GPL-3"

SLOT="0"

KEYWORDS="~amd64"

IUSE=""

PATCHES=(
	"${FILESDIR}/${P}-lv2.patch"
	"${FILESDIR}/${P}-pow10.patch"
)

RDEPEND=">=media-libs/lv2-1.18.8
	>=dev-cpp/gtkmm-3.24.6
	>=sci-libs/fftw-3.3.10"

DEPEND="${RDEPEND}"

BDEPEND="dev-util/cmake
	virtual/pkgconfig"

src_prepare() {
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(-DCMAKE_INSTALL_PREFIX="${EPREFIX}/usr/$(get_libdir)/lv2")
	cmake_src_configure
}
