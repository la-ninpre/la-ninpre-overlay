# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson gnome.org

DESCRIPTION="dock/panel library for GTK 4"

HOMEPAGE="https://gitlab.gnome.org/GNOME/libpanel"

LICENSE="LGPL-3"

SLOT="0"

KEYWORDS="~amd64"

IUSE="vala doc introspection examples"

REQUIRED_USE="doc? ( introspection )"

RDEPEND="
>=dev-libs/glib-2.72
>=gui-libs/gtk-4.6
>=gui-libs/libadwaita-1.0
vala? ( dev-lang/vala )
introspection? ( dev-libs/gobject-introspection )
doc? ( >=dev-util/gi-docgen-2021.1 )
"

DEPEND="${RDEPEND}"

BDEPEND="virtual/pkgconfig
dev-util/cmake
>=dev-util/meson-0.60
dev-util/gtk-doc-am
"

PATCHES=(
	"${FILESDIR}/${P}-gtk-doc-dir.patch"
)

src_configure() {
	local emesonargs=(
		$(meson_use vala vapi)
		$(meson_feature introspection)
		$(meson_feature doc docs)
		$(meson_use examples install-examples)
	)
	meson_src_configure
}
