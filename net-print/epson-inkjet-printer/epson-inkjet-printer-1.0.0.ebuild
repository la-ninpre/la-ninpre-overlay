# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit autotools rpm multilib-minimal

DESCRIPTION="Epson Inkjet Printer Driver (ESC/P)"
HOMEPAGE="https://download.ebz.epson.net/dsc/search/01/search/?OSC=LX"
SRC_URI="http://download.ebz.epson.net/dsc/f/01/00/01/87/87/4bb10f016a8e2e3ec39d55ff72b636c0a340365c/${PN}-201207w-${PV}-1lsb3.2.src.rpm"

S="${WORKDIR}/${PN}-filter-${PV}"

# don't know if it's ok to specify custom variables, but why not
_printer_dir="${WORKDIR}/${PN}-201207w-${PV}"
_cups_libexec_dir="/usr/libexec/cups"

LICENSE="GPL-2 EPSON-EULA"
SLOT="0"
KEYWORDS="amd64"
IUSE="debug"

MULTILIB_COMPAT=( abi_x86_{32,64} )

DEPEND="net-print/cups"
RDEPEND="${DEPEND}"

PATCHES=(
	"${FILESDIR}/${P}-cups-libexec-dir.patch"
	"${FILESDIR}/${P}-resource-dir.patch"
	"${FILESDIR}/${P}-raster.patch"
	"${FILESDIR}/${P}-usrdoc.patch"
)

src_unpack() {
	rpm_src_unpack "${A}"
}

src_prepare() {
	default

	eautoreconf

	sed -i -e "/^\*cupsFilter/ s|/opt/${PN}-201207w/cups/lib|${_cups_libexec_dir}|" \
		-e "s|/opt/${PN}-201207w|/usr/share/${PN}|" \
		${_printer_dir}/ppds/*.ppd

	multilib_copy_sources
}

src_configure() {

	local conf=(
		LDFLAGS="${LDFLAGS} -Wl,--no-as-needed"
		$(use_enable debug)
	)
	econf "${conf[@]}"
}

src_install() {
	default

	if use abi_x86_32; then
		dolib.so "${_printer_dir}/lib/libEpson_201207w.MT.so.1.0.0" \
			"${_printer_dir}/lib/libEpson_201207w.so.1.0.0"

		dosym libEpson_201207w.MT.so.1.0.0 /usr/$(get_libdir)/libEpson_201207w.MT.so.1
		dosym libEpson_201207w.so.1.0.0 /usr/$(get_libdir)/libEpson_201207w.so.1
	fi
	if use abi_x86_64; then
		dolib.so "${_printer_dir}/lib64/libEpson_201207w.MT.so.1.0.0" \
			"${_printer_dir}/lib64/libEpson_201207w.so.1.0.0"

		dosym libEpson_201207w.MT.so.1.0.0 /usr/$(get_libdir)/libEpson_201207w.MT.so.1
		dosym libEpson_201207w.so.1.0.0 /usr/$(get_libdir)/libEpson_201207w.so.1
	fi

	insinto "/usr/share/${PN}"
	doins -r "${_printer_dir}/resource/" \
		"${_printer_dir}/watermark/"

	insinto "/usr/share/ppd/${PN}"
	doins ${_printer_dir}/ppds/*.ppd
}
