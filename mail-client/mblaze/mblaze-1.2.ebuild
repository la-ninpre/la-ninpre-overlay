# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="unix utilities to deal with maildir"

HOMEPAGE="https://github.com/leahneukirchen/mblaze"

SRC_URI="https://github.com/leahneukirchen/mblaze/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="public-domain"

SLOT="0"

KEYWORDS="~amd64"

src_install() {
	emake DESTDIR="${D}" PREFIX="/usr" install
}
