# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3

DESCRIPTION="unix utilities to deal with maildir"

HOMEPAGE="https://github.com/leahneukirchen/mblaze"

EGIT_REPO_URI="https://github.com/leahneukirchen/mblaze.git"
EGIT_BRANCH="master"

LICENSE="public-domain"

SLOT="0"

src_install() {
	emake DESTDIR="${D}" PREFIX="/usr" install
}
