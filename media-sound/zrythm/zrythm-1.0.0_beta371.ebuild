# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson xdg

DESCRIPTION="highly automated and intuitive digital audio workstation"

HOMEPAGE="https://www.zrythm.org"

MY_PV="v1.0.0-beta.3.7.1"
ZIX_PV="afc6ef7e54988fd68f33df21ec2a220e6bfc49f4"
REPROC_PV="14.2.4"
SRC_URI="
	https://git.sr.ht/~alextee/${PN}/archive/${MY_PV}.tar.gz
	https://gitlab.com/drobilla/zix/-/archive/${ZIX_PV}/zix-${ZIX_PV}.tar.gz
	https://github.com/DaanDeMeyer/reproc/archive/refs/tags/v${REPROC_PV}.tar.gz -> reproc-${REPROC_PV}.tar.gz
"

S="${WORKDIR}/${PN}-${MY_PV}"

LICENSE="AGPL-3+"

SLOT="0"

KEYWORDS="~amd64"

IUSE="+X +alsa +bundled-plugins +carla +man +sdl
	graphviz guile jack lsp-dsp opus pulse rtaudio rtmidi test valgrind"

# TODO: handle tests
RESTRICT="!test? ( test )"

RDEPEND="
	=gui-libs/libadwaita-1.2*
	>=dev-libs/json-glib-1
	>=dev-libs/libcyaml-1.2.0
	>=dev-libs/serd-0.30.0
	>=dev-libs/sord-0.14.0
	>=gui-libs/gtk-4
	>=gui-libs/gtksourceview-5
	>=gui-libs/libpanel-1
	>=media-libs/fontconfig-2.13.0
	>=media-libs/graphene-1.0
	>=media-libs/libaudec-0.3.2
	>=media-libs/libsamplerate-0.1.8
	>=media-libs/libsndfile-1.0.25
	>=media-libs/lilv-0.24.6
	>=media-libs/lv2-1.16.0
	>=media-libs/sratom-0.4.0
	>=sci-libs/fftw-3.3.5
	>=x11-libs/pango-1.50
	X? ( x11-libs/libX11 )
	alsa? ( media-libs/alsa-lib )
	app-arch/zstd
	carla? ( media-sound/carla )
	dev-libs/boost
	dev-libs/glib
	dev-libs/libpcre2
	dev-libs/xxhash
	graphviz? ( media-gfx/graphviz )
	guile? ( dev-scheme/guile )
	jack? ( virtual/jack )
	kde-frameworks/breeze-icons
	lsp-dsp? ( >=media-libs/lsp-dsp-lib-0.5.5 )
	media-libs/chromaprint
	media-libs/libepoxy
	media-libs/rubberband
	media-libs/vamp-plugin-sdk
	net-misc/curl
	opus? ( >=media-libs/libsndfile-1.0.29 )
	pulse? ( media-libs/libpulse )
	rtaudio? ( >=media-libs/rtaudio-5.1.0 )
	rtmidi? ( >=media-libs/rtmidi-5.0.0 )
	sdl? ( media-libs/libsdl2 )
	sys-libs/libbacktrace
	valgrind? ( dev-util/valgrind )
"

DEPEND="${RDEPEND}"

BDEPEND="
	virtual/pkgconfig
	dev-util/meson
	dev-lang/sassc
"

PATCHES=(
	# this function is inside of ifdef, so it doesn't exist if HAVE_CARLA is
	# not defined, but it's still used like it's there
	"${FILESDIR}/${P}-carla-ifdef-function.patch"
	# carla has libdir variable, not carla_libdir
	"${FILESDIR}/${P}-carla-libdir.patch"
	# zrythm requires unreleased carla, we don't have that in release
	"${FILESDIR}/${P}-carla-plugin-clap.patch"
	# what?
	"${FILESDIR}/${P}-redundant-logical-or.patch"
)

src_unpack() {
	default
	mv "${WORKDIR}/zix-${ZIX_PV}" "${S}/subprojects/zix"
	mv "${WORKDIR}/reproc-${REPROC_PV}" "${S}/subprojects/reproc"
}

src_configure() {
	local emesonargs=(
		-Db_lto=false
		-Dcheck_updates=false
		$(meson_feature X x11)
		$(meson_feature alsa)
		$(meson_feature carla)
		$(meson_feature graphviz)
		$(meson_feature guile)
		$(meson_feature jack)
		$(meson_feature lsp-dsp lsp_dsp)
		$(meson_feature pulse)
		$(meson_feature rtaudio)
		$(meson_feature rtmidi)
		$(meson_feature sdl)
		$(meson_feature valgrind)
		$(meson_use bundled-plugins bundled_plugins)
		$(meson_use man manpage)
		$(meson_use opus)
		$(meson_use test tests)
	)
	meson_src_configure
}

src_install() {
	meson_src_install --skip-subprojects zix,reproc
}
