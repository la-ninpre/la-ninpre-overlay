# Copyright 1999-2021 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=7

DESCRIPTION="multichannel drum plugin"
HOMEPAGE="http://www.drumgizmo.org"
SRC_URI="http://www.drumgizmo.org/releases/drumgizmo-${PV}/${P}.tar.gz"
LICENSE="LGPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE="cli vst lv2"

DEPEND="
lv2? ( >=media-libs/lv2-1.0 )
>=media-libs/libsndfile-1.0.21
media-libs/zita-resampler
dev-libs/expat
>=dev-ruby/pkg-config-0.23
>=media-libs/libpng-1.2
media-libs/libsmf
dev-libs/libpthread-stubs
dev-util/cppunit
"

RDEPEND="${DEPEND}"

PATCHES=(
	#"${FILESDIR}/${P}_compile.patch"
)

src_configure(){
	econf $(use_enable vst vst) \
		$(use_enable lv2 lv2) \
		$(use_enable cli cli)
}

