# la-ninpre gentoo overlay

personal overlay for packages i really need but that aren't available on other
repos. note the word **personal**. i'll try to maintain it, but i don't promise
that it will be usable at all times.

note that these packages usually aren't thoroughly tested, so use them at your
own risk.

## available packages

* dev-libs/libcyaml
* gui-libs/libpanel
* media-libs/eq10q
* media-libs/libaudec
* media-libs/lsp-dsp-lib
* media-libs/lsp-plugins
* media-sound/carla
* media-sound/drumgizmo
* media-sound/zrythm
* net-im/gajim
* net-print/epson-inkjet-printer (for epson L110, L210, L300, L350, L355, L550, L555 series)

## installing

use `app-portage/eselect-repository` to add this overlay
(see [gentoo wiki page for eselect/repository][1]).
then sync it using `emaint` (see [gentoo wiki page about syncing repos][2]).

```
# eselect repository add la-ninpre git https://git.aaoth.xyz/la-ninpre-overlay.git
# emaint sync -r la-ninpre
```

[1]:https://wiki.gentoo.org/wiki/Eselect/Repository
[2]:https://wiki.gentoo.org/wiki/Ebuild_repository#Repository_synchronization

## contributing

[contact me](https://aaoth.xyz/about.html#contact).
