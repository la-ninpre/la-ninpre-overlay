# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="C library for reading and writing YAML"

HOMEPAGE="https://github.com/tlsa/libcyaml/"

SRC_URI="https://github.com/tlsa/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"

SLOT="0"

KEYWORDS="~amd64"

IUSE="debug"

RDEPEND="dev-libs/libyaml"

DEPEND="${RDEPEND}"

BDEPEND="virtual/pkgconfig"

src_configure() {
	if use debug ; then
		VARIANT=debug
	else
		VARIANT=release
	fi

	export VARIANT
	export PREFIX="${EPREFIX}/usr"
	export LIBDIR="$(get_libdir)"
}
